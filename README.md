# todo-app-frontend

## Introduction

A TypeScript React application for managing a todo list.

Live demo - https://crosskey-challenge-react.gambitgroup.fi/

## Installation

### Prerequisites

- Node & NPM (`^v5.2` for `npx`)

### Production
1. Run `npm install` to install all dependencies
2. Run `npm run css-build` to build the CSS from SCSS.
3. Run `npm run build` to build production ready files, output directory is `./build`
4. Run `npx serve -s build` to run the build files.
5. Go to `http://localhost:5000` in your browser to view the project.

### Development
1. Run `npm install` to install dependencies
2. Run `npm start` to start the dev server, it watches for changes in the `src` folder.
3. Go to `http://localhost:3000` in your browser to view the project.

