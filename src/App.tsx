import React from 'react';
import './App.css';
import {Header} from './screens/shared/components/Header';
import {Footer} from './screens/shared/components/Footer';
import {TodoManager} from './screens/todo-manager/components/TodoManager';
import TextLoop from 'react-text-loop';

export const App: React.FC = () => {
    return (
        <div className="App d-flex flex-column">
            <Header>
                <h1 className="text-white">Todo App</h1>
            </Header>
            <main className="content flex-fill d-flex justify-content-center align-items-center">
                <TodoManager/>
            </main>
            <Footer>
                <h5 className="text-white">Features</h5>
                <TextLoop springConfig={{stiffness: 100, damping: 20}}>
                    <p className="text-white w-100">Drag and drop</p>
                    <p className="text-white w-100">Offline support</p>
                </TextLoop>
            </Footer>
        </div>
    );
};
