import React from 'react';
import {ITodo} from '../types/Todo';

interface Props {
    todo: ITodo;
    deleteTodo?: (id: string) => void;
}

export const TodoListItem: React.FC<Props> = ({todo, deleteTodo, children}) => {
    const renderTodoActions = deleteTodo && (
        <span className="todo-actions d-flex justify-content-around align-items-center">
            <div className="todo-delete-action btn btn-sm btn-outline-danger rounded-pill m-1" onClick={() => deleteTodo(todo.id)}>Delete</div>
        </span>
    );

    return (
        <li id={todo.id} className="TodoListItem list-group-item rounded-0 bg-primary-o-10">
            <div className="d-flex justify-content-between align-items-center">
                <span className="todo-text text-white mr-2">
                    <h6 className="m-1">{todo.text}</h6>
                    {children}
                </span>
                {renderTodoActions}
            </div>
        </li>
    );
};
