import React, {useState, useEffect} from 'react';
import uuidv4 from 'uuid/v4';
import {DragDropContext, Droppable, DropResult} from 'react-beautiful-dnd';
import {TodoList} from './TodoList';
import {AddTodoItem} from './AddTodoItem';
import {ITodo} from '../types/Todo';
import * as todoStorageUtils from '../utils/todoStorageUtils';
import * as dndUtils from '../utils/dragAndDropUtils';

export const TodoManager: React.FC = () => {
    const initialTodos: ITodo[] = todoStorageUtils.getTodosFromLocalStorage();
    const [todos, setTodos] = useState<ITodo[]>(initialTodos);

    /**
     * Add a todo item
     * @param text
     */
    const addTodoItemHandler = (text: string): void => {
        const todo: ITodo = {
            id: uuidv4(),
            text: text,
        };
        setTodos([...todos, todo]);
    };

    /**
     * Delete a todo item by ID
     * @param id
     */
    const deleteTodoItemHandler = (id: string): void => {
        setTodos(todos.filter(todo => todo.id !== id));
    };

    /**
     * Handle drag and drop event for todo item
     * @param result
     */
    const onDragEndHandler = (result: DropResult) => {
        if (!result.destination) {
            return;
        }

        if (result.destination.index === result.source.index) {
            return;
        }

        const sortedTodos: ITodo[] = dndUtils.reorder(
            todos,
            result.source.index,
            result.destination.index
        );

        setTodos(sortedTodos);
    };

    /**
     * Save the todos to local storage every time the todos array changes.
     */
    useEffect((): void => {
        todoStorageUtils.setTodosToLocalStorage(todos)
    }, [todos]);

    return (
        <div className="TodoManager container row flex-fill justify-content-center align-items-center mx-0 px-0">
            <div className="col-12 col-sm-6 my-2">
                <div className="todo-list-wrapper d-flex flex-column justify-content-center align-items-center">
                    <DragDropContext onDragEnd={onDragEndHandler}>
                        <Droppable droppableId="list">
                            {provided => (
                                <div ref={provided.innerRef} {...provided.droppableProps} className="w-100">
                                    <TodoList todos={todos} deleteTodo={deleteTodoItemHandler}/>
                                    {provided.placeholder}
                                </div>
                            )}
                        </Droppable>
                    </DragDropContext>
                </div>
            </div>
            <div className="col-12 col-sm-6 my-2">
                <div className="d-flex flex-column justify-content-center align-items-center">
                    <AddTodoItem addTodoItem={addTodoItemHandler}/>
                </div>
            </div>
        </div>
    );
};
