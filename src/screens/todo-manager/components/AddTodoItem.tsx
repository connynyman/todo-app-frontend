import React, {useState} from 'react';

interface Props {
    addTodoItem: (text: string) => void;
}

export const AddTodoItem: React.FC<Props> = ({addTodoItem}) => {
    const [text, setText] = useState('');
    
    return (
        <div className="AddTodoItem w-100">
            <div className="row justify-content-center align-items-center">
                <div className="col-12">
                    <input
                        id="add-toto-item-input"
                        className="form-control placeholder-white rounded-pill bg-transparent text-white"
                        type="text"
                        aria-describedby="add-todo"
                        placeholder="Enter todo text here"
                        value={text}
                        onChange={(e) => setText(e.target.value)}
                    />
                </div>
                <div className="col-12 my-2">
                    <button className="btn btn-outline-primary rounded-pill w-100"
                            disabled={!text}
                            onClick={() => {
                                addTodoItem(text);
                                setText('');
                            }}>
                        Add todo
                    </button>
                </div>
            </div>
        </div>
    );
};
