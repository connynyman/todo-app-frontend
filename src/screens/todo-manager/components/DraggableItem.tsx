import React from 'react';
import {Draggable} from 'react-beautiful-dnd';

interface Props {
    draggableId: string,
    index: number;
}

export const DraggableItem: React.FC<Props> = ({draggableId, index, children}) => {
    return (
        <Draggable draggableId={draggableId} index={index}>
            {provided => (
                <div ref={provided.innerRef}
                     {...provided.draggableProps}
                     {...provided.dragHandleProps}
                >
                    {children}
                </div>
            )}
        </Draggable>
    );
};
