import React from 'react';
import {TodoListItem} from './TodoListItem';
import {ITodo} from '../types/Todo';
import {DraggableItem} from './DraggableItem';

interface Props {
    todos: ITodo[];
    deleteTodo?: (id: string) => any;
}

export const TodoList: React.FC<Props> = ({todos, deleteTodo, children}) => {
    const placeholderTodo: ITodo = {
        id: 'placeholder-todo',
        text: 'You have no todos'

    };

    const renderTodoList = (
        todos.length
            ? (
                <ul className="list-group">
                    {todos.map((todo, index: number) => (
                        <DraggableItem key={todo.id} draggableId={todo.id} index={index}>
                            <TodoListItem
                                todo={todo}
                                deleteTodo={deleteTodo}
                            />
                        </DraggableItem>
                    ))}
                </ul>
            )
            : <TodoListItem todo={placeholderTodo}/>
    );

    return (
        <div className="TodoList w-100">
            {renderTodoList}
            {children}
        </div>
    );
};
