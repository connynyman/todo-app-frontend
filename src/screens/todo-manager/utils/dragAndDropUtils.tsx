import {ITodo} from '../types/Todo';

export const reorder = (list: ITodo[], startIndex: number, endIndex: number): ITodo[] => {
    const result = Array.from(list);
    const [removed] = result.splice(startIndex, 1);
    result.splice(endIndex, 0, removed);

    return result;
};
