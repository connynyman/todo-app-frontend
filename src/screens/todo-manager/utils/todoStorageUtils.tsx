import {ITodo} from '../types/Todo';

const DEFAULT_STORAGE_KEY = 'todo-list';

export const getTodosFromLocalStorage = (storageKey: string = DEFAULT_STORAGE_KEY): ITodo[] => {
    const todoJson = localStorage.getItem(storageKey);
    return JSON.parse(todoJson || '[]');
};

export const setTodosToLocalStorage = (todos: ITodo[] = [], storageKey: string = DEFAULT_STORAGE_KEY): void => {
    localStorage.setItem(storageKey, JSON.stringify(todos));
};
