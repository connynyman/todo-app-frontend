import React from 'react';

export const Header: React.FC = ({children}) => {
    return (
        <header className="Header">
            <div className="bg-primary-o-10 py-3 py-md-4 py-lg-5">
                <div className="container text-center">
                    {children}
                </div>
            </div>
        </header>
    );
};
