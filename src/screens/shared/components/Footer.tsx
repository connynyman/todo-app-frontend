import React from 'react';

export const Footer: React.FC = ({children}) => {
    return (
        <footer className="Footer">
            <div className="bg-primary-o-10 py-3 py-md-4 py-lg-5">
                <div className="container text-center">
                    {children}
                </div>
            </div>
        </footer>
    );
};
